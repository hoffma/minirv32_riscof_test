#!/usr/bin/env bash

set -x

LOG_LEVEL="info"

# needed binaries
arr=(riscv32-unknown-elf-gcc spike riscv_sim_RV32 ghdl)

wget -q https://share.kode.lol/riscof-toolchain.tar.gz
tar -zxf riscof-toolchain.tar.gz
wget -q https://share.kode.lol/ghdl-bin.tar.gz
tar -C riscof-toolchain -zxf ghdl-bin.tar.gz
export PATH="$(pwd)/riscof-toolchain/bin:$PATH"
export PATH="$(pwd)/riscof-toolchain/ghdl/bin:$PATH"


for element in "${arr[@]}"; do
  elem_path=$(which $element)
  if [ -x "$elem_path" ]; then
    echo "${element} found at: ${elem_path}"
  else
    echo "Executable for '${element} not found"
    exit 1
  fi
done


# python3 -m venv env
# source env/bin/activate
# python3 -m pip install --upgrade pip
# python3 -m pip install riscof

riscof arch-test --clone
riscof validateyaml --config=config.ini
riscof testlist --config=config.ini \
  --suite=riscv-arch-test/riscv-test-suite/ \
  --env=riscv-arch-test/riscv-test-suite/env 

riscof --verbose $LOG_LEVEL run --config=config.ini \
  --suite=riscv-arch-test/riscv-test-suite/ \
  --env=riscv-arch-test/riscv-test-suite/env \
  --no-browser

# check report
if grep -rniq riscof_work/report.html -e '>0failed<'
then
  echo "Test successful!"
  exit 0
else
  echo "Test failed."
  exit 1
fi
