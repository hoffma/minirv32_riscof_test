# RISC-V Compliance Checks

[![riscof-pipeline](https://gitlab.com/hoffma/minirv32_riscof_test/badges/master/pipeline.svg)](https://gitlab.com/hoffma/minirv32_riscof_test/)

This repository is running the [riscof](https://riscof.readthedocs.io/) 
framework on my [minirv32](https://gitlab.com/hoffma/minirv32). The tests are 
heavily inspired by the neorv32 version[^1].

Currently the pre-compiled toolchain is downloaded from my dedicated server.
Maybe this should change in the future.

The toolchain-scripts/ folder contains the bash scripts which were used to
compile the binaries.

- [^1] https://github.com/stnolting/neorv32-riscof/
