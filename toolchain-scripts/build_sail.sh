#!/bin/bash

if [[ $SAIL_INSTALL == "" ]]; then
  SCRIPT_PATH="$(dirname $0)/$(basename $0)"

  echo "[ERROR][build_sail.sh]: SAIL_INSTALL is empty" 1>&2
  echo "Executing script was: ${SCRIPT_PATH}" 1>&2
  exit 1
else
  echo "SAIL_INSTALL is: ${SAIL_INSTALL}"
fi

# Prerequisites
apt-get install -y opam  build-essential libgmp-dev z3 pkg-config zlib1g-dev
mkdir -p $SAIL_INSTALL

opam init -y --disable-sandboxing
opam switch create ocaml-base-compiler.4.06.1
opam install sail -y
eval $(opam config env)
git clone https://github.com/riscv/sail-riscv.git 
cd sail-riscv
git checkout 0.5
OPAMCLI=2.0 make
OPAMCLI=2.0 ARCH=RV32 make
cp -R c_emulator/* $SAIL_INSTALL
