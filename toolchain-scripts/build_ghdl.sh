#!/bin/bash


if [[ $GHDL_INSTALL == "" ]]; then
  SCRIPT_PATH="$(dirname $0)/$(basename $0)"

  echo "[ERROR][build_gcc.sh]: GHDL_INSTALL is empty" 1>&2
  echo "Executing script was: ${SCRIPT_PATH}" 1>&2
  exit 1
else
  echo "GHDL_INSTALL is: ${GHDL_INSTALL}"
fi

# source: https://ghdl.github.io/ghdl/development/building/LLVM.html#build-llvm
# prerequisites
apt-get install -y gcc gnat llvm clang
mkdir -p $GHDL_INSTALL

git clone https://github.com/ghdl/ghdl.git
cd ghdl
mkdir build
cd build
../configure --with-llvm-config --enable-synth --prefix=$GHDL_INSTALL
make -j$(nproc) # build with newlib
make install
