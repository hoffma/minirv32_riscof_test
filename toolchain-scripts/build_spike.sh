#!/bin/bash

if [[ $SPIKE_INSTALL == "" ]]; then
  SCRIPT_PATH="$(dirname $0)/$(basename $0)"

  echo "[ERROR][build_spike.sh]: SPIKE_INSTALL is empty" 1>&2
  echo "Executing script was: ${SCRIPT_PATH}" 1>&2
  exit 1
else
  echo "SPIKE_INSTALL is: ${SPIKE_INSTALL}"
fi

apt-get install -y device-tree-compiler
mkdir -p $SPIKE_INSTALL

git clone https://github.com/riscv-software-src/riscv-isa-sim.git
cd riscv-isa-sim
mkdir build
cd build
../configure --prefix=$SPIKE_INSTALL
make -j$(nproc)
make install
