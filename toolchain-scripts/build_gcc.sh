#!/bin/bash


if [[ $GCC_INSTALL == "" ]]; then
  SCRIPT_PATH="$(dirname $0)/$(basename $0)"

  echo "[ERROR][build_gcc.sh]: GCC_INSTALL is empty" 1>&2
  echo "Executing script was: ${SCRIPT_PATH}" 1>&2
  exit 1
else
  echo "GCC_INSTALL is: ${GCC_INSTALL}"
fi

# source: https://github.com/riscv-collab/riscv-gnu-toolchain
# prerequisites
apt-get install -y autoconf automake autotools-dev curl python3 python3-pip \
  libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo \
  gperf libtool patchutils bc zlib1g-dev libexpat-dev ninja-build git cmake \
  libglib2.0-dev
mkdir -p $GCC_INSTALL

git clone --recursive https://github.com/riscv/riscv-gnu-toolchain
# git clone --recursive https://github.com/riscv/riscv-opcodes.git
cd riscv-gnu-toolchain
# for 32-bit toolchain
# ./configure --prefix=$GCC_INSTALL --with-arch=rv32gc --with-abi=ilp32d
./configure --prefix=$GCC_INSTALL --with-arch=rv32i --with-abi=ilp32 # only rv32i for now
make -j$(nproc) # build with newlib
make install
