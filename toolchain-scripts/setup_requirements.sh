#!/bin/bash

# dependencies 
apt update
RUN apt install -y python3-dev python3-pip
RUN python3 -m pip install --upgrade pip
apt install -y git vim curl build-essential git curl wget
