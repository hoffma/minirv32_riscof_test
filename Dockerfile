FROM ubuntu:22.04

RUN apt-get update
RUN apt install -y git vim curl build-essential git curl wget
RUN apt install -y gcc gnat libgnat-10
# RUN riscof --help
# RUN apt install -y autoconf automake autotools-dev curl python3 \
#   libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo \
#   gperf libtool patchutils bc zlib1g-dev libexpat-dev
# RUN apt-get install -y opam  build-essential libgmp-dev z3 pkg-config zlib1g-dev
# RUN apt-get install -y device-tree-compiler

# RUN apt-get install -y python3-dev python3-pip
# RUN python3 -m pip install --upgrade pip
# RUN python3 -m pip install riscof
# 
# ADD riscof-toolchain /riscof-toolchain
# # ADD run_test.sh /
# # RUN bash run_test.sh
# ADD test_toolchain.sh /
# RUN bash test_toolchain.sh
# RUN riscof --version

# TODO: also get checksum and verify
# wget https://share.kode.lol/riscof-toolchain.checksum


